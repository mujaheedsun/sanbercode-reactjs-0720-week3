import React, {Component} from 'react'
import './Timer.css'

class Timer extends Component{
  constructor(props){
    super(props);
    
    this.state = {
      remove: false,
      time: 100
    }
    
  }

  componentDidMount(){
    if (this.props.start !== undefined && this.props.start > 100) {
      this.setState({time: this.props.start})
    }
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentDidUpdate(prevProps, prevState){
    if (this.state.remove != prevState.remove) {
      this.componentWillUnmount();
      return;

    }if (this.state.time < 0) {
      this.setState({remove : true});
      return;
    }
    
  }


  componentWillUnmount(){
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      time: this.state.time - 1 
    });
  }


  render(){
  let display = new Date().toLocaleTimeString();
    return(
      <>
      {
        this.state.remove === true ? null:
        <div>
          <div className="clock">
            <h1>Sekarang jam : {display}</h1>
          </div>
            
          <div className="timer">
            <h1> Hitung mundur : {this.state.time}</h1>
          </div>
        </div>
      }
      </>
    )}
}

export default Timer