import React from 'react';
import logo from './logo.svg';
import './App.css';
import HargaBuah from './tugas14/HargaBuah';
import Timer from './tugas12/Timer';


function App() {
  return (
    <div className="App">
      <HargaBuah />
      <Timer start={111} />
    </div>
  );
}

export default App;
