import React from 'react'
import './HargaBuah.css'

class HargaBuah extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataHargaBuah : [
        {nama: "Semangka", harga: 10000, berat: 1000},
        {nama: "Anggur", harga: 40000, berat: 500},
        {nama: "Strawberry", harga: 30000, berat: 400},
        {nama: "Jeruk", harga: 30000, berat: 1000},
        {nama: "Mangga", harga: 30000, berat: 500}
      ],
      inputNamaBuah : "",
      inputHargaBuah : "",
      inputBeratBuah : "",
      indexOfForm:-1
    }

    this.handleChangeNama = this.handleChangeNama.bind(this);
    this.handleChangeHarga = this.handleChangeHarga.bind(this);
    this.handleChangeBerat = this.handleChangeBerat.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }
 
  handleEdit(event) { 
    let indexOfForm = event.target.value
    let dataHargaBuah = this.state.dataHargaBuah[indexOfForm]
    this.setState({
      inputNamaBuah: dataHargaBuah.nama,
      inputHargaBuah: dataHargaBuah.harga,
      inputBeratBuah: dataHargaBuah.berat,
      indexOfForm
    })
  }

  handleDelete(event) {
    let indexOfForm = event.target.value
    let newDataHargaBuah = this.state.dataHargaBuah
    newDataHargaBuah.splice(indexOfForm, 1)
    this.setState({dataHargaBuah: newDataHargaBuah})
  }

  handleChangeNama(event) {
    console.log(event.target.value)
    this.setState({inputNamaBuah : event.target.value})
  }

  handleChangeHarga(event) {
    this.setState({inputHargaBuah : event.target.value})
  }

  handleChangeBerat(event) {
    this.setState({inputBeratBuah : event.target.value})
  }

  handleSubmit(event) {
    event.preventDefault();
    let dataHargaBuah = this.state.dataHargaBuah

    if (this.state.indexOfForm >= 0) {
      dataHargaBuah[this.state.indexOfForm] = {
        nama:this.state.inputNamaBuah,
        harga:this.state.inputHargaBuah,
        berat:this.state.inputBeratBuah}
    } else {
      dataHargaBuah = [...dataHargaBuah, {
        nama:this.state.inputNamaBuah,
        harga:this.state.inputHargaBuah,
        berat:this.state.inputBeratBuah}]
    }

    this.setState({
      dataHargaBuah : dataHargaBuah,
      inputBeratBuah:"",
      inputNamaBuah:"",
      inputHargaBuah:"",
      indexOfForm:-1
    })
  }


  render() {
    return (
      <>
        <div>
          <h1>Tabel Harga Buah</h1>
          <div>
            <table>
              <th className='Nama'> Nama</th>
              <th className='Harga'> Harga</th>
              <th className='Berat'> Berat</th>
              <th> Aksi </th>

              {this.state.dataHargaBuah.map((buah, index) => {
                return (
                  <tr key={index}>
                    <td className='Nama'>{buah.nama}</td>
                    <td className='Harga'>{buah.harga}</td>
                    <td className='Berat'>{buah.berat / 1000} kg</td>
                    <td>
                      <button onClick={this.handleEdit} value={index}>Edit
                      </button>
                    </td>
                    <td>
                      <button onClick={this.handleDelete} value={index}>Delete
                      </button>
                    </td>
                  </tr>
                )
              })}
            </table>

              <h1> Form Harga Buah Baru</h1>
              <form onSubmit={this.handleSubmit}>
                <label>Masukkan nama buah : </label>
                <input type="text" value={this.state.inputNamaBuah} onChange={this.handleChangeNama}/><br/>

                <label>Masukkan harga buah : </label>
                <input type="text" value={this.state.inputHargaBuah} onChange={this.handleChangeHarga}/><br/>

                <label>Masukkan berat buah : </label>
                <input type="text" value={this.state.inputBeratBuah} onChange={this.handleChangeBerat}/><br/> 

                <input type="submit" value="Submit" />
              </form>
          </div>
        </div>
      </>
    )
  }
}

export default HargaBuah