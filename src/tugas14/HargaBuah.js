import React, { useState, useEffect } from 'react'
import './HargaBuah.css'
import axios from 'axios'

const HargaBuah = () => {
  
  // const [dataHargaBuah, setDataHargaBuah] = useState([
    //   {nama: "Semangka", harga: 10000, berat: 1000},
    //   {nama: "Anggur", harga: 40000, berat: 500},
    //   {nama: "Strawberry", harga: 30000, berat: 400},
    //   {nama: "Jeruk", harga: 30000, berat: 1000},
    //   {nama: "Mangga", harga: 30000, berat: 500}
    // ]);
    
  const [dataHargaBuah, setDataHargaBuah] = useState(null);
  const [inputNamaBuah, setInputNamaBuah] = useState("");
  const [inputHargaBuah, setInputHargaBuah] = useState("");
  const [inputBeratBuah, setInputBeratBuah] = useState("");
  const [indexOfForm, setIndexOfForm] = useState(-1);

  useEffect(() => {
    if (dataHargaBuah === null) {
      axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
      .then(res => {
        console.log(res.data)
        setDataHargaBuah(res.data.map(el => { 
          return {nama: el.name, harga: el.price, berat:el.weight, id: el.id} 
        }))
      })
    }
  })
  const handleChangeNama = (event) => {
    setInputNamaBuah(event.target.value)
  }

  const handleChangeHarga = (event) => {
    setInputHargaBuah(event.target.value)
  }

  const handleChangeBerat = (event) => {
    setInputBeratBuah(event.target.value)
  }

  const handleEdit = (event) => {
    console.log(dataHargaBuah)
    let idBuah = event.target.value
    let editDataHargaBuah = dataHargaBuah.find(x => x.id == idBuah)
    console.log(editDataHargaBuah)
    setInputNamaBuah(editDataHargaBuah.nama)
    setInputHargaBuah(editDataHargaBuah.harga)
    setInputBeratBuah(editDataHargaBuah.berat)
    setIndexOfForm(idBuah)
  }

  const handleDelete = (event) => {
    let idBuah = event.target.value
    let newDataHargaBuah = [...dataHargaBuah].filter(el => el.id !== idBuah )

    axios.delete(`http://backendexample.sanbercloud.com/api/fruits`, {id:idBuah})
      .then(res => {
        console.log(res)  
      })

    setDataHargaBuah(newDataHargaBuah)
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    let newDataHargaBuah = dataHargaBuah

    if (indexOfForm >= 0) {
      newDataHargaBuah[indexOfForm] = {
        nama:inputNamaBuah,
        harga:inputHargaBuah,
        berat:inputBeratBuah}
    } else {
      newDataHargaBuah = [...newDataHargaBuah, {
        nama:inputNamaBuah,
        harga:inputHargaBuah,
        berat:inputBeratBuah}]
    }

    axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name: inputNamaBuah, price:inputHargaBuah, weight:inputBeratBuah})
      .then(res => {
        console.log(res)  
      })

    setDataHargaBuah(newDataHargaBuah)
    setInputBeratBuah("")
    setInputNamaBuah("")
    setInputHargaBuah("")
    setIndexOfForm(-1)
  }


  return (
    <>
      <div>
        <h1>Tabel Harga Buah</h1>
        <div>
          <thead>
            <tr>
              <th className='Nama'> Nama</th>
              <th className='Harga'> Harga</th>
              <th className='Berat'> Berat</th>
              <th> Aksi </th>
            </tr>
          </thead>
          <table>

            {
              (dataHargaBuah !== null) && dataHargaBuah.map((buah, index) => {
                return (
                  <tr key={index}>
                    <td className='Nama'>{buah.nama}</td>
                    <td className='Harga'>{buah.harga}</td>
                    <td className='Berat'>{buah.berat / 1000} kg</td>
                    <td>
                      <button onClick={handleEdit} value={buah.id}>Edit
                      </button>
                    </td>
                    <td>
                      <button onClick={handleDelete} value={buah.id}>Delete
                      </button>
                    </td>
                  </tr>
                )
              })
            }
            
          </table>

            <h1> Form Harga Buah Baru</h1>
            <form onSubmit={handleSubmit}>
              <label>Masukkan nama buah : </label>
              <input type="text" value={inputNamaBuah} onChange={handleChangeNama}/><br/>

              <label>Masukkan harga buah : </label>
              <input type="text" value={inputHargaBuah} onChange={handleChangeHarga}/><br/>

              <label>Masukkan berat buah : </label>
              <input type="text" value={inputBeratBuah} onChange={handleChangeBerat}/><br/> 

              <input type="submit" value="Submit" />
            </form>
        </div>
      </div>
    </>
  )
}

export default HargaBuah
